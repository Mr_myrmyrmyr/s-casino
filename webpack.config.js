const path = require('path')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
    context: path.resolve(__dirname, 'src'),
    mode: 'development',
    entry: './index.js',
    output: {
        filename: '[name].[contenthash].js',
        path: path.resolve(__dirname, 'dist')
    },
    devServer: {
       port: 4200 
    },
    plugins: [
        new HTMLWebpackPlugin({
            template: './pug/index.pug',
        }),
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: '[name].[contenthash].css'
        })
    ],
    module: {
        rules: [
            {
                test: /\.sass$/,
                use: ['style-loader',
                MiniCssExtractPlugin.loader, {
                    loader: 'css-loader'
                },
                {
                    loader: 'sass-loader'
                }
                ]
            },
            {
                test: /\.(png|jpg|svg|gif)$/,
                exclude: path.resolve(__dirname, './src/assets/fonts'),
                use: [
                    {
                      loader: 'file-loader',
                      options: {
                        emitFile: true,
                        name: '[name].[ext]'
                      }
                    }
                ]
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                    },
                },
            },
            {
                test: /\.xml$/,
                use: ['xml-loader']
            },
            {
                test: /\.pug$/,
                exclude: /node_modules/,
                loader: 'pug-loader'
            }
        ],
    }
}